_Last updated: 21 August 2022_
# Privacy Policy

## Introduction
I get that legal documents can be overwhelming and boring, so i've tried my best to keep it as simple and possible while providing the information needed. It is important however to remember that these terms are legally binding between yourself and me if you choose to use VerificationSystem, so please read them carefully before proceeding - I will continue to improve and tackle some sections.

## Your Privacy
Your privacy is important to me and therefore I do not sell or give any of your data to third parties and will always inform you about how your data is used within this service in this Privacy Policy.

### What data is collected?
The collected data is kept to an absolute minimum, aswell as being encrypted with AES-CBC-256, in addition to the VPS only being accesible by Public/Private Key Auth.

This data includes:
- **Server Id's**: To store server settings
- **Message Id's of messages the bot itself sends**: To know what answers a certain user gave
- **User Id's**: To know who to give roles to on accept or kick/ban on deny

### Who is this data shared with?
I will NEVER sell your data to any third-parties, although i use certain service providers to process your data, such as:

- [IONOS](https://ionos.com/) who provide the VPS that hosts the bot
- [Google Firebase](https://firebase.google.com) who provide the Database

### Where is my information stored?
Your information is currently stored on servers, located in a datacenter in Baden-Baden, Baden-Württemberg, Germany,
as such, your data is not only subject to the data protection guidelines of Europe, but also to the very strict German Federal Data Protection Act.

If you wish to request deletion of any information stored, simply kick the bot from the server and it will delete all of the associated data.
